<h1 align="center">Welcome to PokemonApp 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> An app to collect Pokemon and view your collection

### 🏠 [https://pokemonpp.herokuapp.com/](https://pokemonpp.herokuapp.com/)

## Usage

```sh
ng serve
```
Overriding the default 4200 port:
```sh
ng serve --port 3000
```

## Run tests

```sh
ng t
```

Usage:
- Login to the page
- Click on Pokemon in the Collection page to add them to your collection
- View your collection in the Trainer page
- Logout to clear your session


Limitations:
- Uncollecting Pokemon required page refreshes,
  therefore this user-friendly feature has been ommitted
- App relies on local storage
- All information is lost on logout (intended for current development purposes, but future versions should aim to make the app more user-friendly with an external authentication server)


***
_END OF README FILE_
