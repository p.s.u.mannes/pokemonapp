import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  //Flag to dynamically display HTML logout link
  loggedIn = false;

  constructor(private router: Router) {}

  //Check if user is logged in for loggedIn flag
  ngOnInit(): void {
    if (localStorage.getItem('username')) {
      this.loggedIn = true;
    }
  }

  //Resetting the app by clearing local storage
  //and redirecting the user back to the login form
  resetApp(): void {
    console.log('clearing local storage');
    this.loggedIn = false;
    localStorage.clear();
    this.router.navigate(['/goodbye']);
  }
}
