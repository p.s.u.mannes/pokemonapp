// TESTING FILE

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorNotExistComponent } from './error-not-exist.component';

describe('ErrorNotExistComponent', () => {
  let component: ErrorNotExistComponent;
  let fixture: ComponentFixture<ErrorNotExistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ErrorNotExistComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorNotExistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
