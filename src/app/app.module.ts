//importing modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//Importing components
import { AppComponent } from './app.component';
import { LoginPage } from './login/login.page';
import { HeaderComponent } from './components/header/header.component';
import { CollectionPage } from './collection/collection.page';
import { TrainerPage } from './trainer/trainer.page';
import { ErrorNotExistComponent } from './components/error-not-exist/error-not-exist.component';
import { GoodbyeComponent } from './goodbye/goodbye.component';

//Importing authentication guard and routing
import { AuthGuard } from './authenticator/auth.guard';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  //Decorator

  //components are added under declarations
  declarations: [
    AppComponent,
    LoginPage,
    HeaderComponent,
    CollectionPage,
    TrainerPage,
    ErrorNotExistComponent,
    GoodbyeComponent,
  ],

  //imports are added under modules
  imports: [BrowserModule, HttpClientModule, AppRoutingModule, FormsModule],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
