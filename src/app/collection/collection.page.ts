import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.page.html',
  styleUrls: ['./collection.page.css'],
})
export class CollectionPage implements OnInit {
  pokemons: any[] = [];
  collection: string[] = [];

  //injecting service inside constructor
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    //initialize collection array by splitting
    //string of collection from local storage into an array
    if (localStorage.getItem('collection')) {
      this.collection = localStorage.getItem('collection')!.split(',');
    }

    //using dataService service to fetch the Pokemon header
    //description (includes name) from the PokeAPI
    this.dataService.getPokemonHeaders().subscribe((response: any) =>
      response.results.forEach((result: { name: string }) =>
        this.dataService
          .getPokemonData(result.name)
          //Using the name element of previous fetch result
          //to subscribe to another fetch, this time retrieving
          //detailed data from the PokeAPI using the name
          .subscribe((uniqueResponse: any) => {
            this.pokemons.push(uniqueResponse);
            console.log(this.pokemons);
          })
      )
    );
  }

  addToCollection(name: string): void {
    //if collection does not include pokemon name
    if (!this.collection.includes(name)) {
      //add the name to the collection variable
      this.collection.push(name);
      //finally, set the new value of local storage
      localStorage.setItem('collection', this.collection.join(','));
    }
  }
}
