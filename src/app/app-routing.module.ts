import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login/login.page';
import { CollectionPage } from './collection/collection.page';
import { TrainerPage } from './trainer/trainer.page';
import { ErrorNotExistComponent } from './components/error-not-exist/error-not-exist.component';
import { AuthGuard } from './authenticator/auth.guard';
import { GoodbyeComponent } from './goodbye/goodbye.component';

const routes: Routes = [
  {
    //The main login page
    //with login form
    path: '',
    pathMatch: 'full',
    component: LoginPage,
  },
  {
    //Authentication guarded collection page
    //Must be logged in to access
    path: 'collection',
    pathMatch: 'full',
    component: CollectionPage,
    canActivate: [AuthGuard],
  },
  {
    //Authentication guarded trainer page
    //Must be logged in to access
    path: 'trainer',
    pathMatch: 'full',
    component: TrainerPage,
    canActivate: [AuthGuard],
  },
  {
    // A simple goodbye page after logging out
    path: 'goodbye',
    pathMatch: 'full',
    component: GoodbyeComponent,
  },
  // Error 404 page with link back to home
  // in case user types in invalid url
  { path: '**', component: ErrorNotExistComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
