import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
  constructor() {}
  collection: string[] = [];
  amountCollected: number = 0;

  ngOnInit(): void {
    if (localStorage.getItem('collection')) {
      this.collection = localStorage.getItem('collection')!.split(',');
      this.amountCollected = this.collection.length;
    }
  }
}
