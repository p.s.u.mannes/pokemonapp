import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  //Initializing variables

  //[()] Two-way input for name input in form
  nameInput = '';

  //Current username
  currentUser = '';

  //Logical flag to show/hide login form
  loggedIn = false;

  //Logical flag to show loading text after form submission
  isLoading = false;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.isLoading = false;
    if (localStorage.getItem('username')) {
      console.log('user is logged in');
      this.loggedIn = true;
      this.currentUser = localStorage.getItem('username')!;
    }
  }

  //function onSubmit returns nothing
  //used for html form in login.page.html
  public onSubmit(createForm: NgForm): void {
    console.log(createForm.valid);
    console.log('creating username');
    localStorage.setItem('username', this.nameInput);
    console.log(localStorage.getItem('username'));
    //making sure user knows their page is loading...
    this.isLoading = true;
    //reload page to update header
    location.reload();
  }

  // handleClear function
  // @params: form
  // Completely resets the login form and current
  // input in the name input field
  handleClear(form: NgForm) {
    this.nameInput = ' ';
    form.resetForm();
  }
}
