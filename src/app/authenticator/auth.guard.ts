// Authentication guard stops user
// from visiting pages they do not have privilege to access

import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    //if user is logged in, return true to grant page access
    if (this.authService.isUserLoggedIn()) {
      return true;
    } else {
      //if not logged in, prompt the user to login
      //and redirect them to the login page...
      //return false to deny page access.
      window.alert('Permission denied: login please.');
      this.router.navigate(['/']);
      return false;
    }
  }
}
