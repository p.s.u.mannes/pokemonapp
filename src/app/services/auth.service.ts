//Service used by AuthGuard to verify
//if user can access privileged pages
//that require the user to be logged into the app

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  //Logic to determine if the user
  //is logged in
  isUserLoggedIn() {
    if (localStorage.getItem('username')) {
      return true;
    } else {
      return false;
    }
  }
}
