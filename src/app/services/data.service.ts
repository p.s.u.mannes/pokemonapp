// Service responsible for fetching
// data from Pokemon API

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  //Limit to API requests
  limit = 10;

  constructor(private http: HttpClient) {}

  //get initial pokemon data (just the names)
  getPokemonHeaders() {
    return this.http.get(
      `https://pokeapi.co/api/v2/pokemon?limit=${this.limit}`
    );
  }

  //get more pokemon data (image data)
  // @params: name = name of pokemon
  getPokemonData(name: string) {
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
  }
}
